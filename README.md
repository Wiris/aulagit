<!-- Parte inicial -->
## incia um novo repositório
git init
## mostra as alterações
git status
## adiciona um arquivo
git add <arquivo>
## adiciona todos os arquivos
git add .
## faz o commit
git commit
## mostra os commits
git log
## mostra os commits de forma menos verbosa
git log --oneline
## realiza o commit com mensagem
git commit -m "<mensagem>"
## adiciona os arquivos e realiza o commit com mensagem
git commit -am "<mensagem>"
## adiciona um repositório remoto
git remote add <nome> <link>
## envia as alterações da branch atual para a branch do repositório remoto
git push <repositório-remoto> <branch-no-remoto>
## envia as alterações da branch atual para o repositório remoto
git push
## baixa as alterações da branch do repositório remoto
git pull <repositório-remoto> <branch-no-remoto>
## baixa as alterações do repositório remoto
git pull
## clona um repositório remoto existente
git clone <link>

<!-- Intermediário -->
## cria e muda de branch baseado na outra branch
git checkout -b <nome-da-branch>
## muda de branch
git checkout <branch>
## Lista todas as branches
git branch 
## Deleta branch se tiver sido mergeada com outra
git branch -d <nome-da-branch> 
## Deleta branch sem verificar se está mergeada
git branch -D <nome-da-branch> 
## faz o merge de uma branch para a atual 
git merge <branch>

<!-- Configuração -->
## Nome do usuário
git config --global user.name "Nome" 
## Email do usuário
git config --global user.email "email@email.com" 
## Editor
git config --global core.editor "vim" 
## Branch padrão
git config --global init.defaultBranch main 